package miro

import (
	"path"
	"strconv"
	"time"

	http "github.com/valyala/fasthttp"
)

// GetLogs allows Company Admins to track changes made within the company team. They come in extremely useful when you
// need to troubleshoot a problem or to get a detailed report of important events (e.g. changes to the global security
// settings, invitations of new users, new boards created, etc.).
//
// App that uses audit logs must be installed by Company Admin.
func (m *Miro) GetLogs(createdAfter, createdBefore time.Time, offset, limit int) ([]*Event, int, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	a.Add("createdAfter", createdAfter.Format(time.RFC3339Nano))
	a.Add("createdBefore", createdBefore.Format(time.RFC3339Nano))
	a.Add("offset", strconv.Itoa(offset))
	a.Add("limit", strconv.Itoa(limit))

	dst, err := m.get(path.Join("audit", "logs"), a)
	if err != nil {
		return nil, 0, err
	}

	result := new(Pagination)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, 0, err
	}

	list := make([]*Event, 0)
	if err := m.parser.Unmarshal(result.Data, &list); err != nil {
		return nil, 0, err
	}

	return list, result.Size, nil
}
