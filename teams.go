package miro

import (
	"path"
	"strconv"
	"strings"

	http "github.com/valyala/fasthttp"
)

type (
	updateTeamRequest struct {
		Name string `json:"name"`
	}

	inviteToTeamRequest struct {
		Emails []string `json:"emails"`
	}
)

// GetTeam returns an Team Object if the team exists and is available to the user.
func (m *Miro) GetTeam(id string, fields ...string) (*Team, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.get(path.Join("teams", id), a)
	if err != nil {
		return nil, err
	}

	result := new(Team)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// UpdateTeam updates Team.
func (m *Miro) UpdateTeam(id, newName string, fields ...string) (*Team, error) {
	body := new(updateTeamRequest)
	body.Name = newName

	src, err := m.parser.Marshal(body)
	if err != nil {
		return nil, err
	}

	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.patch(src, path.Join("teams", id), a)
	if err != nil {
		return nil, err
	}

	result := new(Team)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

//nolint: lll
// GetTeamUserConnections returns a collection of Team User Connection which represent users in this team.
func (m *Miro) GetTeamUserConnections(id string, offset, limit int, fields ...string) ([]*TeamUserConnection, int, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("offset", strconv.FormatInt(int64(offset), 10))
		a.Add("limit", strconv.FormatInt(int64(limit), 10))
	}

	dst, err := m.get(path.Join("teams", id, "user-connections"), a)
	if err != nil {
		return nil, 0, err
	}

	result := new(Pagination)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, 0, err
	}

	list := make([]*TeamUserConnection, 0)
	if err := m.parser.Unmarshal(result.Data, &list); err != nil {
		return nil, 0, err
	}

	return list, result.Size, nil
}

// GetTeamCurrentUserConnection returns a Team User Connection Object for the current user.
func (m *Miro) GetTeamCurrentUserConnection(id string) (*TeamUserConnection, error) {
	dst, err := m.get(path.Join("teams", id, "user-connections", "me"), nil)
	if err != nil {
		return nil, err
	}

	result := new(TeamUserConnection)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// InviteToTeam add Users to the team with role member by it's emails.
//
// If there are registered users with these emails, they will be added to the team and notified about it.
// Otherwise, User Objects with state not_registered will be created automatically and added to the team. Unregistered
// users will also receive invitations to register.
//
// Please note, that payment at the end of the period may be increased because of new team members.
func (m *Miro) InviteToTeam(id string, emails []string) ([]*TeamUserConnection, int, error) {
	body := new(inviteToTeamRequest)
	body.Emails = emails

	src, err := m.parser.Marshal(body)
	if err != nil {
		return nil, 0, err
	}

	dst, err := m.patch(src, path.Join("teams", id, "invite"), nil)
	if err != nil {
		return nil, 0, err
	}

	result := make([]*TeamUserConnection, 0)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, 0, err
	}

	return result, len(result), nil
}
