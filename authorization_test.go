package miro_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAuthorization(t *testing.T) {
	result, err := client.GetAuthorization()
	assert.NoError(t, err)

	if !assert.NotNil(t, result) {
		t.FailNow()
	}

	os.Setenv("MIRO_TEAM_ID", result.Team.ID)
	os.Setenv("MIRO_USER_ID", result.User.ID)
}
