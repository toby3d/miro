//nolint: dupl
package miro

// GetBoardUserConnection returns a User Connection for Board.
func (m *Miro) GetBoardUserConnection(id string, fields ...string) (*BoardUserConnection, error) {
	dst, err := m.getUserConnection("board", id, fields...)
	if err != nil {
		return nil, err
	}

	result := new(BoardUserConnection)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// UpdateBoardUserConnection updates User Connection for Board.
//
// The role of the board owner cannot be changed, unless the board owner assigns ownership of the board to another
// user.
//
// Once another user becomes the board owner, the previous owner becomes an editor.
func (m *Miro) UpdateBoardUserConnection(id, newRole string, fields ...string) (*BoardUserConnection, error) {
	dst, err := m.updateUserConnection("board", id, newRole, fields...)
	if err != nil {
		return nil, err
	}

	result := new(BoardUserConnection)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// DeleteBoardUserConnection remove User Connection for Board.
//
// You can not delete the Board User Connection object for the owner role.
func (m *Miro) DeleteBoardUserConnection(id string) (bool, error) {
	return m.deleteUserConnection("board", id)
}
