package miro

import (
	"path"
	"strings"

	http "github.com/valyala/fasthttp"
)

type updateCurrentUserRequest struct {
	Name string `json:"name"`
}

func (m *Miro) GetUser(id string, fields ...string) (*User, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.get(path.Join("users", id), a)
	if err != nil {
		return nil, err
	}

	u := new(User)
	if err = m.parser.Unmarshal(dst, u); err != nil {
		return nil, err
	}

	return u, nil
}

func (m *Miro) GetCurrentUser(fields ...string) (*User, error) {
	return m.GetUser("me", fields...)
}

func (m *Miro) UpdateCurrentUser(newName string, fields ...string) (*User, error) {
	body := new(updateCurrentUserRequest)
	body.Name = newName

	src, err := m.parser.Marshal(body)
	if err != nil {
		return nil, err
	}

	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.patch(src, path.Join("users", "me"), a)
	if err != nil {
		return nil, err
	}

	u := new(User)
	if err = m.parser.Unmarshal(dst, u); err != nil {
		return nil, err
	}

	return u, nil
}
