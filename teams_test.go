package miro_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetTeam(t *testing.T) {
	teamID, ok := os.LookupEnv("MIRO_TEAM_ID")
	if !ok {
		TestGetAuthorization(t)

		teamID = os.Getenv("MIRO_TEAM_ID")
	}

	result, err := client.GetTeam(teamID)
	assert.NoError(t, err)
	assert.NotNil(t, result)
}

func TestUpdateTeam(t *testing.T) {
	teamID, ok := os.LookupEnv("MIRO_TEAM_ID")
	if !ok {
		TestGetAuthorization(t)

		teamID = os.Getenv("MIRO_TEAM_ID")
	}

	result, err := client.UpdateTeam(teamID, "Dev team")
	assert.NoError(t, err)
	assert.NotNil(t, result)
}

func TestGetTeamUserConnections(t *testing.T) {
	teamID, ok := os.LookupEnv("MIRO_TEAM_ID")
	if !ok {
		TestGetAuthorization(t)

		teamID = os.Getenv("MIRO_TEAM_ID")
	}

	result, count, err := client.GetTeamUserConnections(teamID, 0, 10)
	assert.NoError(t, err)
	assert.NotZero(t, count)
	assert.NotNil(t, result)
}

func TestGetTeamCurrentUserConnection(t *testing.T) {
	teamID, ok := os.LookupEnv("MIRO_TEAM_ID")
	if !ok {
		TestGetAuthorization(t)

		teamID = os.Getenv("MIRO_TEAM_ID")
	}

	result, err := client.GetTeamCurrentUserConnection(teamID)
	assert.NoError(t, err)
	assert.NotNil(t, result)

	os.Setenv("MIRO_TEAM_USER_CONNECTION_ID", result.ID)
}

func TestInviteToTeam(t *testing.T) {
	teamID, ok := os.LookupEnv("MIRO_TEAM_ID")
	if !ok {
		TestGetAuthorization(t)

		teamID = os.Getenv("MIRO_TEAM_ID")
	}

	userEmail, ok := os.LookupEnv("MIRO_USER_EMAIL")
	if !ok {
		t.SkipNow()
	}

	result, count, err := client.InviteToTeam(teamID, []string{userEmail})
	assert.NoError(t, err)
	assert.NotZero(t, count)
	assert.NotNil(t, result)
}
