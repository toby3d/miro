package miro

import (
	"bytes"
	"path"
	"strconv"
	"strings"

	http "github.com/valyala/fasthttp"
)

type ShareBoard struct {
	Emails                 []string `json:"emails"`
	Role                   string   `json:"role"`
	Message                string   `json:"message"`
	TeamInvitationStrategy string   `json:"teamInvitationStrategy"`
}

// CreateBoard creates a new Board.
func (m *Miro) CreateBoard(req Board, fields ...string) (*Board, error) {
	src, err := m.parser.Marshal(&req)
	if err != nil {
		return nil, err
	}

	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.post(src, "boards", a)
	if err != nil {
		return nil, err
	}

	result := new(Board)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// GetBoard returns a Board.
func (m *Miro) GetBoard(id string, fields ...string) (*Board, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.get(path.Join("boards", id), a)
	if err != nil {
		return nil, err
	}

	result := new(Board)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// GetBoardUserConnections returns a current User Board Connections.
func (m *Miro) GetBoardUserConnections(id string, offset, limit int) ([]*BoardUserConnection, int, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)
	a.Add("offset", strconv.Itoa(offset))
	a.Add("limit", strconv.Itoa(limit))

	dst, err := m.get(path.Join("boards", id, "user-connections"), a)
	if err != nil {
		return nil, 0, err
	}

	result := new(Pagination)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, 0, err
	}

	list := make([]*BoardUserConnection, 0)
	if err := m.parser.Unmarshal(result.Data, &list); err != nil {
		return nil, 0, err
	}

	return list, result.Size, nil
}

// ShareBoard share invites to Board.
func (m *Miro) ShareBoard(id string, req ShareBoard) ([]*BoardUserConnection, int, error) {
	src, err := m.parser.Marshal(&req)
	if err != nil {
		return nil, 0, err
	}

	dst, err := m.post(src, path.Join("boards", id, "share"), nil)
	if err != nil {
		return nil, 0, err
	}

	if bytes.Equal(dst, []byte("[ ]")) {
		return nil, 0, nil
	}

	result := new(Pagination)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, 0, err
	}

	list := make([]*BoardUserConnection, 0)
	if err := m.parser.Unmarshal(result.Data, &list); err != nil {
		return nil, 0, err
	}

	return list, result.Size, nil
}

// UpdateBoard updates Board.
func (m *Miro) UpdateBoard(id string, req Board, fields ...string) (*Board, error) {
	src, err := m.parser.Marshal(&req)
	if err != nil {
		return nil, err
	}

	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.patch(src, path.Join("boards", id), a)
	if err != nil {
		return nil, err
	}

	result := new(Board)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// GetCurrentUserBoards returns collection of available Boards.
func (m *Miro) GetCurrentUserBoards(id string, offset, limit int, fields ...string) ([]*Board, int, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("offset", strconv.Itoa(offset))
		a.Add("limit", strconv.Itoa(limit))
	}

	dst, err := m.get(path.Join("teams", id, "boards"), a)
	if err != nil {
		return nil, 0, err
	}

	result := new(Pagination)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, 0, err
	}

	list := make([]*Board, 0)
	if err := m.parser.Unmarshal(result.Data, &list); err != nil {
		return nil, 0, err
	}

	return list, result.Size, nil
}
