package miro

import (
	"path"
	"strings"

	http "github.com/valyala/fasthttp"
)

type updateUserConnectionRequest struct {
	Role string `json:"role"`
}

func (m *Miro) getUserConnection(t, id string, fields ...string) ([]byte, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.get(path.Join(t+"-user-connections", id), a)
	if err != nil {
		return nil, err
	}

	return dst, nil
}

func (m *Miro) updateUserConnection(t, id, newRole string, fields ...string) ([]byte, error) {
	body := new(updateUserConnectionRequest)
	body.Role = newRole

	src, err := m.parser.Marshal(body)
	if err != nil {
		return nil, err
	}

	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.patch(src, path.Join(t+"-user-connections", id), a)
	if err != nil {
		return nil, err
	}

	return dst, nil
}

func (m *Miro) deleteUserConnection(t, id string) (bool, error) {
	err := m.delete(path.Join(t+"-user-connections", id))
	return err == nil, err
}
