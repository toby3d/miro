package miro_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetBoardUserConnection(t *testing.T) {
	boardUserConnectionID, ok := os.LookupEnv("MIRO_BOARD_USER_CONNECTION_ID")
	if !ok {
		TestGetBoardUserConnections(t)

		boardUserConnectionID = os.Getenv("MIRO_BOARD_USER_CONNECTION_ID")
	}

	result, err := client.GetBoardUserConnection(boardUserConnectionID)
	assert.NoError(t, err)

	if !assert.NotNil(t, result) {
		t.FailNow()
	}

	assert.Equal(t, boardUserConnectionID, result.ID)
	assert.Equal(t, os.Getenv("MIRO_USER_ID"), result.User.ID)
}

func TestUpdateBoardUserConnection(t *testing.T) {
	boardUserConnectionID, ok := os.LookupEnv("MIRO_BOARD_USER_CONNECTION_ID")
	if !ok {
		TestGetBoardUserConnections(t)

		boardUserConnectionID = os.Getenv("MIRO_BOARD_USER_CONNECTION_ID")
	}

	result, err := client.UpdateBoardUserConnection(boardUserConnectionID, "owner")
	assert.NoError(t, err)

	if !assert.NotNil(t, result) {
		t.FailNow()
	}

	assert.Equal(t, "owner", result.Role)
}

func TestDeleteBoardUserConnection(t *testing.T) {
	// NOTE(toby3d): You can not delete the Board User Connection object for the owner role.
	/*
		boardUserConnectionID, ok := os.LookupEnv("MIRO_BOARD_USER_CONNECTION_ID")
		if !ok {
			t.SkipNow()
		}

		ok, err := client.DeleteBoardUserConnection(boardUserConnectionID)
		assert.NoError(t, err)
		assert.True(t, ok)
	*/
}
