module gitlab.com/toby3d/miro

go 1.13

require (
	github.com/json-iterator/go v1.1.8
	github.com/stretchr/testify v1.3.0
	github.com/valyala/fasthttp v1.6.0
	golang.org/x/oauth2 v0.0.0-20191122200657-5d9234df094c
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898
)
