package miro

import (
	"path"

	http "github.com/valyala/fasthttp"
)

type (
	Collection struct {
		Type string    `json:"type"` // collection
		Data []*Widget `json:"data"`
		Size int       `json:"size"`
	}

	// Widget returns information about the available widgets on your Miro Board.
	// You can create, edit, delete and get widgets on your Boards.
	Widget struct {
		// Each type of widget has its own specific fields, check Widget Types
		Type string `json:"type,omitempty"`

		// Unique identifier for the widget
		ID string `json:"id,omitempty"`

		// An object which manages UI capabilities of the widget, e.g. you can disable manual widget editing.
		Capabilities *Capabilities `json:"capabilities"`

		// Application metadata on the widget
		Metadata Metadata `json:"metadata"`

		// Date and time when the widget was created
		CreatedAt *DateTime `json:"createdAt,omitempty"`

		// Date and time when the widget was modified
		ModifiedAt *DateTime `json:"modifiedAt,omitempty"`

		// The user who first created the widget
		CreatedBy *User `json:"createdBy,omitempty"`

		// The user who last modified the widget
		ModifiedBy *User `json:"modifiedBy,omitempty"`

		// optional
		X           float64                `json:"x,omitempty"`
		Y           float64                `json:"y,omitempty"`
		Date        string                 `json:"date,omitempty"`
		Description string                 `json:"description,omitempty"`
		Height      float64                `json:"height,omitempty"`
		Rotation    float64                `json:"rotation,omitempty"`
		Scale       float64                `json:"scale,omitempty"`
		Text        string                 `json:"text,omitempty"`
		Title       string                 `json:"title,omitempty"`
		Width       float64                `json:"width,omitempty"`
		Style       map[string]interface{} `json:"style,omitempty"`
		StartWidget *WidgetPoint           `json:"startWidget,omitempty"`
		EndWidget   *WidgetPoint           `json:"endWidget,omitempty"`
		Assignee    *Assignee              `json:"assignee,omitempty"`
	}

	Assignee struct {
		UserID string `json:"userId"`
	}

	WidgetPoint struct {
		ID string `json:"id"`
	}

	// Capabilities manages UI capabilities for the widget.
	Capabilities struct {
		Editable bool `json:"editable"`
	}

	// Application metadata provides a way for you to store your own custom data on widgets. This can be used to
	// keep track of a custom state alongside a widget, hold a link to a related item in another system, and
	// various other things.
	//
	// You can write metadata only for the app id which is bounded to your token.
	//
	// Metadata is available via REST API and SDK as well.
	// Please note that metadata was designed to store a small amount of data. It is limited by 6KB of data per
	// widget for all applications.
	//
	// Metadata field is shared for reading across all apps which have access to board widgets
	Metadata interface{}
)

const (
	WidgetTypeSticker string = "sticker"
	WidgetTypeShape   string = "shape"
	WidgetTypeText    string = "text"
	WidgetTypeLine    string = "line"
	WidgetTypeCard    string = "card"

	// readonly
	WidgetTypeImage         = "image"
	WidgetTypeWebScreenshot = "webscreenshot"
	WidgetTypeDocument      = "document"
	WidgetTypePaint         = "paint"
	WidgetTypePreview       = "preview"
	WidgetTypeEmbed         = "embed"
	WidgetTypeMockup        = "mockup"
	WidgetTypeFrame         = "frame"
	WidgetTypeKanban        = "kanban"
	WidgetTypeUSM           = "usm"
)

// ListAllWidgets has no pagination and is limited to 1000 widgets for the board.
// So you will get only the first 1000 widgets from the board. This is enough for most cases.
func (m *Miro) ListAllWidgets(id string, widgetType string) ([]*Widget, int, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if widgetType != "" {
		a.Add("widgetType", widgetType)
	}

	dst, err := m.get(path.Join("boards", id, "widgets"), a)
	if err != nil {
		return nil, 0, err
	}

	result := new(Collection)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, 0, err
	}

	return result.Data, result.Size, nil
}

// GetWidget get a specific single widget by id.
func (m *Miro) GetWidget(id, widgetID string) (*Widget, error) {
	dst, err := m.get(path.Join("boards", id, "widgets", widgetID), nil)
	if err != nil {
		return nil, err
	}

	result := new(Widget)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// CreateWidget create a single widget.
func (m *Miro) CreateWidget(id string, body Widget) (*Widget, error) {
	src, err := m.parser.Marshal(&body)
	if err != nil {
		return nil, err
	}

	dst, err := m.post(src, path.Join("boards", id, "widgets"), nil)
	if err != nil {
		return nil, err
	}

	result := new(Widget)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// UpdateWidget update a single widget by id. Create a single widget.
func (m *Miro) UpdateWidget(id, widgetID string, body Widget) (*Widget, error) {
	src, err := m.parser.Marshal(&body)
	if err != nil {
		return nil, err
	}

	dst, err := m.patch(src, path.Join("boards", id, "widgets", widgetID), nil)
	if err != nil {
		return nil, err
	}

	result := new(Widget)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// DeleteWidget delete a single widget by id.
func (m *Miro) DeleteWidget(id, widgetID string) (bool, error) {
	err := m.delete(path.Join("boards", id, "widgets", widgetID))
	return err == nil, err
}
