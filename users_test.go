package miro_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetUser(t *testing.T) {
	userID, ok := os.LookupEnv("MIRO_USER_ID")
	if !ok {
		TestGetCurrentUser(t)

		userID = os.Getenv("MIRO_USER_ID")
	}

	result, err := client.GetUser(userID)
	assert.NoError(t, err)
	assert.NotNil(t, result)
}

func TestGetCurrentUser(t *testing.T) {
	result, err := client.GetCurrentUser()
	assert.NoError(t, err)

	if !assert.NotNil(t, result) {
		t.FailNow()
	}

	os.Setenv("MIRO_USER_ID", result.ID)
}

func TestUpdateCurrentUser(t *testing.T) {
	result, err := client.UpdateCurrentUser("Maxim Lebedev")
	assert.NoError(t, err)

	if !assert.NotNil(t, result) {
		t.FailNow()
	}

	assert.Equal(t, "Maxim Lebedev", result.Name)
}
