package miro_test

import (
	"log"
	"os"
	"testing"

	"gitlab.com/toby3d/miro"
	"golang.org/x/oauth2"
)

var client *miro.Miro //nolint: gochecknoglobals

func TestMain(m *testing.M) {
	accessToken, ok := os.LookupEnv("MIRO_ACCESS_TOKEN")
	if !ok {
		log.Println("Miro package tests require application access_token")
		os.Exit(1)
	}

	var err error
	if client, err = miro.NewMiro(oauth2.StaticTokenSource(&oauth2.Token{
		TokenType:   "Bearer",
		AccessToken: accessToken,
	})); err != nil {
		os.Exit(1)
	}

	os.Exit(m.Run())
}
