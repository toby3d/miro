package miro

import (
	"bytes"
	"io"
	"mime/multipart"
	"os"
	"path"

	json "github.com/json-iterator/go"
	http "github.com/valyala/fasthttp"
	"golang.org/x/oauth2"
)

// Miro is the main object on behalf of which all requests are made.
type Miro struct {
	parser     json.API
	httpClient *http.Client
	token      *oauth2.Token
}

// NewMiro creates a new main Miro object by input AccessToken.
func NewMiro(ts oauth2.TokenSource) (*Miro, error) {
	t, err := ts.Token()
	if err != nil {
		return nil, err
	}

	return &Miro{
		parser:     json.ConfigFastest,
		httpClient: &http.Client{Name: "Miro"},
		token:      t,
	}, nil
}

func (m *Miro) get(url string, a *http.Args) ([]byte, error) {
	req := http.AcquireRequest()
	defer http.ReleaseRequest(req)
	req.Header.SetMethod(http.MethodGet)

	return m.request(req, url, a)
}

func (m *Miro) post(src []byte, url string, a *http.Args) ([]byte, error) {
	req := http.AcquireRequest()
	defer http.ReleaseRequest(req)
	req.Header.SetMethod(http.MethodPost)
	req.Header.SetContentType("application/json")
	req.SetBody(src)

	return m.request(req, url, a)
}

func (m *Miro) patch(src []byte, url string, a *http.Args) ([]byte, error) {
	req := http.AcquireRequest()
	defer http.ReleaseRequest(req)
	req.Header.SetMethod(http.MethodPatch)
	req.Header.SetContentType("application/json")
	req.SetBody(src)

	return m.request(req, url, a)
}

func (m *Miro) delete(url string) (err error) {
	req := http.AcquireRequest()
	defer http.ReleaseRequest(req)
	req.Header.SetMethod(http.MethodDelete)

	_, err = m.request(req, url, nil)

	return err
}

func (m *Miro) upload(file *os.File, url string, a *http.Args) ([]byte, error) {
	buff := new(bytes.Buffer)
	w := multipart.NewWriter(buff)

	fw, err := w.CreateFormFile("image", file.Name())
	if err != nil {
		return nil, err
	}

	if _, err = io.Copy(fw, file); err != nil {
		return nil, err
	}

	if err = w.Close(); err != nil {
		return nil, err
	}

	req := http.AcquireRequest()
	defer http.ReleaseRequest(req)
	req.Header.SetMethod(http.MethodPost)
	req.Header.SetContentType("multipart/form-data")
	req.Header.SetMultipartFormBoundary(w.Boundary())
	req.SetBody(buff.Bytes())

	return m.request(req, url, a)
}

func (m *Miro) request(req *http.Request, url string, a *http.Args) ([]byte, error) {
	if m.token != nil {
		req.Header.Set(http.HeaderAuthorization, m.token.Type()+" "+m.token.AccessToken)
	}

	u := http.AcquireURI()
	defer http.ReleaseURI(u)
	u.SetScheme("https")
	u.SetHost("api.miro.com")
	u.SetPath(path.Join("v1", url))

	if a != nil {
		a.CopyTo(u.QueryArgs())
	}

	req.SetRequestURIBytes(u.FullURI())

	resp := http.AcquireResponse()
	defer http.ReleaseResponse(resp)

	var err error
	if err = m.httpClient.Do(req, resp); err != nil {
		return nil, err
	}

	if resp.StatusCode() < http.StatusOK || resp.StatusCode() >= http.StatusBadRequest {
		e := new(Error)
		if err = m.parser.Unmarshal(resp.Body(), e); err != nil {
			return nil, err
		}

		return nil, e
	}

	return resp.Body(), nil
}
