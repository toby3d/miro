# GoLang bindings for the Miro API
> This project is just to provide a wrapper around the API without any additional features.

All methods and types available and this library (possibly) is ready for use in production. :tada:

## Start using telegraph
Download and install it:

```bash
$ go get -u gitlab.com/toby3d/miro
```

Import it in your code:

```bash
import "gitlab.com/toby3d/miro"
```

## Need help?
* [Open new issue](https://gitlab.com/toby3d/miro/issues/new)
* [Discuss in Discord](https://discord.gg/8t6dJhX)