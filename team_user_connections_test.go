package miro_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetTeamUserConnection(t *testing.T) {
	teamUserConnectionID, ok := os.LookupEnv("MIRO_TEAM_USER_CONNECTION_ID")
	if !ok {
		TestGetTeamCurrentUserConnection(t)

		teamUserConnectionID = os.Getenv("MIRO_TEAM_USER_CONNECTION_ID")
	}

	result, err := client.GetTeamUserConnection(teamUserConnectionID)
	assert.NoError(t, err)
	assert.NotNil(t, result)
}

func TestUpdateTeamUserConnection(t *testing.T) {
	teamUserConnectionID, ok := os.LookupEnv("MIRO_TEAM_USER_CONNECTION_ID")
	if !ok {
		TestGetTeamCurrentUserConnection(t)

		teamUserConnectionID = os.Getenv("MIRO_TEAM_USER_CONNECTION_ID")
	}

	result, err := client.UpdateTeamUserConnection(teamUserConnectionID, "admin")
	if !assert.NoError(t, err) {
		assert.FailNow(t, err.Error())
	}

	assert.NotNil(t, result)
}

func TestDeleteTeamUserConnection(t *testing.T) {
	// NOTE(toby3d): The account must have at least one admin
	/*
		teamUserConnectionID, ok := os.LookupEnv("MIRO_TEAM_USER_CONNECTION_ID")
		if !ok {
			TestGetTeamCurrentUserConnection(t)

			teamUserConnectionID = os.Getenv("MIRO_TEAM_USER_CONNECTION_ID")
		}

		ok, err := client.DeleteTeamUserConnection(teamUserConnectionID)
		assert.NoError(t, err)
		assert.True(t, ok)
	*/
}
