package miro_test

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetPicture(t *testing.T) {
	if _, ok := os.LookupEnv("MIRO_PICTURE_ID"); !ok {
		TestCreateOrUpdatePicture(t)
	}

	for _, tc := range []struct {
		message     string
		pictureType string
		id          string
		size        int
		redirect    bool
		expFail     bool
		expEmpty    bool
	}{{
		message:     "Get board picture",
		pictureType: "boards",
		id:          os.Getenv("MIRO_BOARD_ID"),
		redirect:    false,
		size:        420,
		expFail:     false,
		expEmpty:    false,
	}, {
		message:     "Get user picture",
		pictureType: "users",
		id:          os.Getenv("MIRO_USER_ID"),
		redirect:    false,
		size:        420,
		expFail:     false,
		expEmpty:    false,
	}, { // NOTE(toby3d): DevTeam doesn't have team picture
		message:     "Get team picture",
		pictureType: "teams",
		id:          os.Getenv("MIRO_TEAM_ID"),
		redirect:    false,
		size:        420,
		expFail:     false,
		expEmpty:    true,
	}} {
		tc := tc
		t.Run(tc.message, func(t *testing.T) {
			if tc.id == "" {
				t.SkipNow()
			}

			result, err := client.GetPicture(tc.pictureType, tc.id, tc.redirect, tc.size)

			if tc.expEmpty {
				assert.Nil(t, result)
			} else {
				assert.NotNil(t, result)
			}

			if tc.expFail {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestCreateOrUpdatePicture(t *testing.T) {
	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	root, err := os.Getwd()
	assert.NoError(t, err)

	picturePath := filepath.Join(root, "test", "picture.png")
	if _, err = os.Stat(picturePath); os.IsNotExist(err) {
		t.SkipNow()
	}

	f, err := os.Open(picturePath)
	if !assert.NoError(t, err) {
		assert.FailNow(t, err.Error())
	}

	defer func() { assert.NoError(t, f.Close()) }()

	p, err := client.CreateOrUpdatePicture(f, "boards", boardID)
	assert.NoError(t, err)

	if !assert.NotNil(t, p) {
		t.FailNow()
	}

	os.Setenv("MIRO_PICTURE_ID", p.ID)
}

func TestDeletePicture(t *testing.T) {
	if _, ok := os.LookupEnv("MIRO_PICTURE_ID"); !ok {
		t.SkipNow()
	}

	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	ok, err := client.DeletePicture("boards", boardID)
	assert.NoError(t, err)
	assert.True(t, ok)

	os.Unsetenv("MIRO_PICTURE_ID")
}
