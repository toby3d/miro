package miro

import (
	"path"
	"strings"

	http "github.com/valyala/fasthttp"
)

// GetAuthorization returns current Authorization Object.
func (m *Miro) GetAuthorization(fields ...string) (*Authorization, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.get(path.Join("oauth-token"), a)
	if err != nil {
		return nil, err
	}

	result := new(Authorization)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}
