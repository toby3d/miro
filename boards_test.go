package miro_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/toby3d/miro"
)

func TestCreateBoard(t *testing.T) {
	if _, ok := os.LookupEnv("MIRO_BOARD_ID"); ok {
		t.SkipNow()
	}

	t.Run("invalid", func(t *testing.T) {
		result, err := client.CreateBoard(miro.Board{
			Name:        "",
			Description: "Just a testing of invalid board",
			SharingPolicy: &miro.SharingPolicy{
				Access:     "invalidAccess",
				TeamAccess: "invalidTeamAccess",
			},
		})
		assert.EqualError(t, err, "400 Method arguments are not valid")
		assert.Nil(t, result)
	})
	t.Run("valid", func(t *testing.T) {
		result, err := client.CreateBoard(miro.Board{
			Name:        "Valid Board",
			Description: "Just a testing of valid board",
			SharingPolicy: &miro.SharingPolicy{
				Access:     "view",
				TeamAccess: "edit",
			},
		})
		assert.NoError(t, err)

		if !assert.NotNil(t, result) {
			t.FailNow()
		}

		os.Setenv("MIRO_BOARD_ID", result.ID)
	})
}

func TestGetBoard(t *testing.T) {
	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	t.Run("invalid", func(t *testing.T) {
		result, err := client.GetBoard("wtf")
		assert.EqualError(t, err, "404 Board not found")
		assert.Nil(t, result)
	})
	t.Run("valid", func(t *testing.T) {
		result, err := client.GetBoard(boardID)
		assert.NoError(t, err)
		assert.NotNil(t, result)
	})
}

func TestGetBoardUserConnections(t *testing.T) {
	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	list, count, err := client.GetBoardUserConnections(boardID, 0, 10)
	assert.NoError(t, err)
	assert.NotZero(t, count)

	if !assert.NotEmpty(t, list) {
		t.FailNow()
	}

	os.Setenv("MIRO_BOARD_ID", list[0].Board.ID)
	os.Setenv("MIRO_BOARD_USER_CONNECTION_ID", list[0].ID)
}

func TestShareBoard(t *testing.T) {
	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	userEmail, ok := os.LookupEnv("MIRO_USER_EMAIL")
	if !ok {
		t.SkipNow()
	}

	list, count, err := client.ShareBoard(boardID, miro.ShareBoard{
		Emails:                 []string{userEmail},
		Message:                "This is a test invition message, please ignore",
		Role:                   "editor",
		TeamInvitationStrategy: "invite_when_required",
	})
	assert.NoError(t, err)
	assert.NotZero(t, count)

	if !assert.NotEmpty(t, list) {
		t.FailNow()
	}

	os.Setenv("MIRO_USER_EMAIL", list[0].User.Email)
}

func TestUpdateBoard(t *testing.T) {
	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	result, err := client.UpdateBoard(boardID, miro.Board{
		Name:        "Example name",
		Description: "hackme",
	})
	assert.NoError(t, err)
	assert.NotEqual(t, "Valid Board", result.Name)
	assert.NotEqual(t, "Just a testing of valid board", result.Description)
	assert.Equal(t, "view", result.SharingPolicy.Access)
	assert.Equal(t, "edit", result.SharingPolicy.TeamAccess)
}

func TestGetCurrentUserBoards(t *testing.T) {
	teamID, ok := os.LookupEnv("MIRO_TEAM_ID")
	if !ok {
		TestGetAuthorization(t)

		teamID = os.Getenv("MIRO_TEAM_ID")
	}

	list, count, err := client.GetCurrentUserBoards(teamID, 0, 10)
	assert.NoError(t, err)

	if os.Getenv("MIRO_BOARD_ID") != "" {
		assert.NotZero(t, count)
		assert.NotEmpty(t, list)

		return
	}

	assert.Zero(t, count)
	assert.Empty(t, list)
}
