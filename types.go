package miro

import (
	"encoding/json"
	"fmt"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/xerrors"
)

type (
	// Error indicate the status of the request.
	Error struct {
		Type string `json:"type"` // error

		// The HTTP status of the response
		Status int `json:"status"`

		// A short string describing the kind of error that occurred
		Code string `json:"code"`

		// Additional context information describing the error
		Context *Context `json:"context"`

		// A human-readable message describing the error
		Message string `json:"message"`

		frame xerrors.Frame
	}

	// Context is a additional context information describing the error.
	Context struct {
		Fiends []*Field `json:"fields"`
	}

	// Field represent a single field of Context.
	Field struct {
		Field   string `json:"field"`
		Reason  string `json:"reason"`
		Message string `json:"message"`
	}

	// DateTime formatted as RFC 3339
	DateTime struct{ time.Time }

	// Pagination contains a large number of objects in response to a query.
	// To limit the number of objects returned, an offset-based pagination mechanism is used.
	//
	// The maximum limit is 100 items.
	//
	// The returned number of objects in the collection can be less than the requested one, even if offset + limit
	// has not reached the value size.
	//
	// Since the collection can change between requests, sometimes the final number of objects received can differ
	// from the size field.
	Pagination struct {
		Type string `json:"type"` // list

		// Array of items
		Data json.RawMessage `json:"data"`

		// The total size of the collection
		Size int `json:"size"`

		// The offset from the beginning of the collection
		Offset int `json:"offset"`

		// The limit on returned items
		Limit int `json:"limit"`

		// The link to the next page of the collection
		NextLink string `json:"nextLink"`

		// The link to the previous page of the collection
		PrevLink string `json:"prevLink"`
	}

	// Board returns information about the available boards in your Miro account.
	// You can create and get boards, get board invites and board collaborators, and share, update, and delete
	// boards.
	Board struct {
		Type string `json:"type"` // board

		// Unique identifier for the object
		ID string `json:"id"`

		// Date and time when the board was created
		CreatedAt *DateTime `json:"createdAt"`

		// Date and time when the board was modified
		ModifiedAt *DateTime `json:"modifiedAt"`

		// The user who first created the board
		CreatedBy *User `json:"createdBy"`

		// The user who last modified the board
		ModifiedBy *User `json:"modifiedBy"`

		// The user who owns the board
		Owner *User `json:"owner"`

		// Name of the board
		Name string `json:"name"`

		// Description of the board
		Description string `json:"description"`

		// Picture of the board
		Picture *Picture `json:"picture"`

		// The link to view the board in a browser
		ViewLink string `json:"viewLink"`

		SharingPolicy *SharingPolicy `json:"sharingPolicy"`

		// The connection object of the current user in a context and the board. Can be null if there is no
		// relationship between user and board.
		CurrentUserConnection *BoardUserConnection `json:"currentUserConnection"`
	}

	SharingPolicy struct {
		// The global access level of the board.
		Access string `json:"access"`

		// The team access level of the board.
		TeamAccess string `json:"teamAccess"`
	}

	// BoardUserConnection defines a relationship between the user and the board. The user has different
	// permissions on the board depending on their role.
	BoardUserConnection struct {
		Type string `json:"type"` // board-user-connection

		Board *Board `json:"board"`

		// Unique identifier for the object
		ID string `json:"id"`

		// The user that is granted access
		User *User `json:"user"`

		Role string `json:"role"`

		// When the connection was created
		CreatedAt DateTime `json:"createdAt"`

		// When the connection was modified. If it has not been modified, this field is equal to createdAt
		ModifiedAt DateTime `json:"modifiedAt"`

		// The user who created this connection
		CreatedBy User `json:"createdBy"`

		// The user who modified this connection. If it has not been modified, this field is equal to createdBy
		ModifiedBy User `json:"modifiedBy"`
	}

	// Team contains information, you can also get the boards associated with a team, a list of your team's
	// members and invites, and invite others to join your team.
	Team struct {
		Type string `json:"type"` // team

		ID string `json:"id"`

		// Date and time when the team was created
		CreatedAt *DateTime `json:"createdAt"`

		// Date and time when the team was modified. If team has not been modified, this field is equal to createdAt
		ModifiedAt *DateTime `json:"modifiedAt"`

		// The user who created the team
		CreatedBy *User `json:"createdBy"`

		// The user who modified the team. If a team has not been modified, this field is equal to createdBy
		ModifiedBy *User `json:"modifiedBy"`

		Name string `json:"name"`

		Picture *Picture `json:"picture"`
	}

	// TeamUserConnection defines the relationship between the user and the team. Various actions on the team are
	// available, depending on the user's role.
	TeamUserConnection struct {
		Type string `json:"type"` // team-user-connection

		// Unique identifier for the object
		ID string `json:"id"`

		// The user to which access has been granted
		User *User `json:"user"`

		// The team granting access
		Team *Team `json:"team"`

		// Values can be non_team, member or admin
		Role string `json:"role"`

		// When the connection was created
		CreatedAt *DateTime `json:"createdAt"`

		// When the connection was modified. If it has not been modified, this field is equal to createdAt
		ModifiedAt *DateTime `json:"modifiedAt"`

		// The user who created the connection
		CreatedBy *User `json:"createdBy"`

		// The user who modified the connection. If it has not been modified, this field is equal to createdBy
		ModifiedBy *User `json:"modifiedBy"`
	}

	// User contains information about active RealtimeBoard users.
	User struct {
		Type string `json:"type"` // user

		// Identifier for user
		ID string `json:"id"`

		CreatedAt *DateTime `json:"createdAt"`

		// The name of the user (maximum length — 60 chars)
		Name string `json:"name"`

		// The user’s company name (maximum length — 60 chars)
		Company string `json:"company"`

		// The user's role inside a company
		Role string `json:"role"`

		// The company's industry
		Industry string `json:"industry"`

		// The user's linked email address
		Email string `json:"email"`

		// This field can take one of the following values: registered, not_registered, temporary, deleted.
		State string `json:"state"`

		Picture *Picture `json:"picture"`
	}

	// Picture contains information about profile pictures of active RealtimeBoard users.
	Picture struct {
		Type string `json:"type"` // picture

		ID string `json:"id"`

		ImageURL string `json:"imageUrl"`
	}

	// Event represent a single item of Logs collection.
	Event struct {
		Type string `json:"type"` // event

		ID string `json:"id"`

		// The type of event occurred
		Event string `json:"event"`

		Details map[string]interface{} `json:"details"`

		CreatedAt *DateTime `json:"createdAt"`

		// The user who triggered the event
		CreatedBy *User `json:"createdBy"`

		// The specifies which object was affected by the event
		Object interface{} `json:"object"`

		// The connection of the event to specific Organization and Team, with the appropriate ID
		Context *Context `json:"context"`
	}

	// Authorization represents information about your current authorization token.
	Authorization struct {
		Type string `json:"type"` // oAuthToken

		// Unique identifier for the object
		ID string `json:"id"`

		// When the authorization was created
		CreatedAt *DateTime `json:"createdAt"`

		// By whom the authorization was created
		CreatedBy *User `json:"createdBy"`

		// The team where the application was authorized
		Team *Team `json:"team"`

		// The user who authorized the application
		User *User `json:"user"`

		// Scopes that was granted for this authorization
		Scopes []string `json:"scopes"`
	}
)

const (
	TypeAuthorization       string = "oAuthToken"
	TypeBoard               string = "board"
	TypeBoardUserConnection string = "board-user-connection"
	TypeError               string = "error"
	TypePagination          string = "list"
	TypePicture             string = "picture"
	TypeTeam                string = "team"
	TypeTeamUserConnection  string = "team-user-connection"
	TypeUser                string = "user"
)

const (
	ScopeBoardsRead    string = "boards:read"
	ScopeBoardsWrite   string = "boards:write"
	ScopeTeamRead      string = "team:read"
	ScopeTeamWrite     string = "team:write"
	ScopeIdentityRead  string = "identity:read"
	ScopeIdentityWrite string = "identity:write"
	ScopeAuditlogsRead string = "auditlogs:read"
)

//nolint: lll
const (
	EventAccountCreated                                       string = "account_created"
	EventAccountDefaultBoardSharingSettingsChanged            string = "account_default_board_sharing_settings_changed"
	EventAccountDefaultProjectSharingSettingsChanged          string = "account_default_project_sharing_settings_changed"
	EventAccountDeleted                                       string = "account_deleted"
	EventAccountExternalCollaboratorsDisabled                 string = "account_external_collaborators_disabled"
	EventAccountExternalCollaboratorsEnabled                  string = "account_external_collaborators_enabled"
	EventAccountIntegrationSettingsChanged                    string = "account_integration_settings_changed"
	EventAccountInvitationSettingsChanged                     string = "account_invitation_settings_changed"
	EventAccountLogoChanged                                   string = "account_logo_changed"
	EventAccountLogoRemoved                                   string = "account_logo_removed"
	EventAccountNameChanged                                   string = "account_name_changed"
	EventAccountSignUpDomainListChanged                       string = "account_sign_up_domain_list_changed"
	EventAccountSignUpModeChanged                             string = "account_sign_up_mode_changed"
	EventBoardAccessed                                        string = "board_accessed"
	EventBoardAccountSharingDisabled                          string = "board_account_sharing_disabled"
	EventBoardAccountSharingEnabled                           string = "board_account_sharing_enabled"
	EventBoardAccountSharingPermissionChanged                 string = "board_account_sharing_permission_changed"
	EventBoardAddedToProject                                  string = "board_added_to_project"
	EventBoardCoverChanged                                    string = "board_cover_changed"
	EventBoardCreated                                         string = "board_created"
	EventBoardDeleted                                         string = "board_deleted"
	EventBoardDescriptionChanged                              string = "board_description_changed"
	EventBoardMovedFromAccount                                string = "board_moved_from_account"
	EventBoardMovedToAccount                                  string = "board_moved_to_account"
	EventBoardNameChanged                                     string = "board_name_changed"
	EventBoardOpened                                          string = "board_opened"
	EventBoardOwnerChanged                                    string = "board_owner_changed"
	EventBoardPublicLinkDisabled                              string = "board_public_link_disabled"
	EventBoardPublicLinkEnabled                               string = "board_public_link_enabled"
	EventBoardPublicLinkPermissionChanged                     string = "board_public_link_permission_changed"
	EventBoardRemovedFromProject                              string = "board_removed_from_project"
	EventBoardRestored                                        string = "board_restored"
	EventBoardSharedForCommenting                             string = "board_shared_for_commenting"
	EventBoardSharedForEditing                                string = "board_shared_for_editing"
	EventBoardSharedForViewing                                string = "board_shared_for_viewing"
	EventBoardSlackChannelChanged                             string = "board_slack_channel_changed"
	EventBoardSlackDisabled                                   string = "board_slack_disabled"
	EventBoardSlackEnabled                                    string = "board_slack_enabled"
	EventBoardUnshared                                        string = "board_unshared"
	EventOrganizationAccountVisibilityRestrictionDisabled     string = "organization_account_visibility_restriction_disabled"
	EventOrganizationAccountVisibilityRestrictionEnabled      string = "organization_account_visibility_restriction_enabled"
	EventOrganizationDeleted                                  string = "organization_deleted"
	EventOrganizationLogoChanged                              string = "organization_logo_changed"
	EventOrganizationLogoRemoved                              string = "organization_logo_removed"
	EventOrganizationNameChanged                              string = "organization_name_changed"
	EventOrganizationSessionIdleTimeoutDisabled               string = "organization_session_idle_timeout_disabled"
	EventOrganizationSessionIdleTimeoutEnabled                string = "organization_session_idle_timeout_enabled"
	EventOrganizationSharingViaPublicLinkDisabled             string = "organization_sharing_via_public_link_disabled"
	EventOrganizationSharingViaPublicLinkEnabled              string = "organization_sharing_via_public_link_enabled"
	EventOrganizationSharingWithExternalCollaboratorsDisabled string = "organization_sharing_with_external_collaborators_disabled"
	EventOrganizationSharingWithExternalCollaboratorsEnabled  string = "organization_sharing_with_external_collaborators_enabled"
	EventOrganizationSsoDisabled                              string = "organization_sso_disabled"
	EventOrganizationSsoEnabled                               string = "organization_sso_enabled"
	EventOrganizationSsoSettingsChanged                       string = "organization_sso_settings_changed"
	EventOrganizationWhitelistedDomainsDisabled               string = "organization_whitelisted_domains_disabled"
	EventOrganizationWhitelistedDomainsEnabled                string = "organization_whitelisted_domains_enabled"
	EventOrganizationWhitelistedDomainsListChanged            string = "organization_whitelisted_domains_list_changed"
	EventProjectAccountSharingDisabled                        string = "project_account_sharing_disabled"
	EventProjectAccountSharingEnabled                         string = "project_account_sharing_enabled"
	EventProjectCreated                                       string = "project_created"
	EventProjectDeleted                                       string = "project_deleted"
	EventProjectOwnerChanged                                  string = "project_owner_changed"
	EventProjectRenamed                                       string = "project_renamed"
	EventProjectSharedComment                                 string = "project_shared_comment"
	EventProjectSharedEdit                                    string = "project_shared_edit"
	EventProjectSharedView                                    string = "project_shared_view"
	EventProjectUnshared                                      string = "project_unshared"
	EventSignInFailed                                         string = "sign_in_failed"
	EventSignInSucceeded                                      string = "sign_in_succeeded"
	EventSignOutSucceeded                                     string = "sign_out_succeeded"
	EventTemplateCreated                                      string = "template_created"
	EventTemplateDeleted                                      string = "template_deleted"
	EventTemplateOpened                                       string = "template_opened"
	EventTemplateRestored                                     string = "template_restored"
	EventUserConfirmedNewEmailAddress                         string = "user_confirmed_new_email_address"
	EventUserConvertedToOrgExternal                           string = "user_converted_to_org_external"
	EventUserDeactivated                                      string = "user_deactivated"
	EventUserDemotedFromAccountAdmin                          string = "user_demoted_from_account_admin"
	EventUserDemotedFromOrgAdmin                              string = "user_demoted_from_org_admin"
	EventUserInvitationToAccountRemoved                       string = "user_invitation_to_account_removed"
	EventUserInvitedToAccount                                 string = "user_invited_to_account"
	EventUserJoinedToAccount                                  string = "user_joined_to_account"
	EventUserJoinedToOrganization                             string = "user_joined_to_organization"
	EventUserLicenseConvertedToFull                           string = "user_license_converted_to_full"
	EventUserLicenseConvertedToOccasional                     string = "user_license_converted_to_occasional"
	EventUserLocked                                           string = "user_locked"
	EventUserProfileChanged                                   string = "user_profile_changed"
	EventUserPromotedToAccountAdmin                           string = "user_promoted_to_account_admin"
	EventUserPromotedToOrgAdmin                               string = "user_promoted_to_org_admin"
	EventUserReactivated                                      string = "user_reactivated"
	EventUserRemovedFromAccount                               string = "user_removed_from_account"
	EventUserRemovedFromOrganization                          string = "user_removed_from_organization"
	EventUserRequestedEmailAddressChange                      string = "user_requested_email_address_change"
	EventUserUnlocked                                         string = "user_unlocked"
)

var (
	ErrInvalidParameters error = Error{
		Status: 400,
		Code:   "invalidParameters",
		Type:   TypeError,
	}

	ErrTokenNotProvided error = Error{
		Status: 401,
		Code:   "tokenNotProvided",
		Type:   TypeError,
	}

	ErrTokenNotFound error = Error{
		Status: 401,
		Code:   "tokenNotFound",
		Type:   TypeError,
	}

	ErrInsuffucuentPeermissions error = Error{
		Status: 403,
		Code:   "insuffucuentPeermissions",
		Type:   TypeError,
	}

	ErrNotFound error = Error{
		Status: 404,
		Code:   "notFound",
		Type:   TypeError,
	}

	ErrInternalError error = Error{
		Status: 500,
		Code:   "internalError",
		Type:   TypeError,
	}
)

// Endpoint is Miro's OAuth 2.0 endpoint.
//nolint: gochecknoglobals
var Endpoint = oauth2.Endpoint{
	AuthURL:   "https://miro.com/oauth/authorize",
	TokenURL:  "https://api.miro.com/v1/oauth/token",
	AuthStyle: oauth2.AuthStyleAutoDetect,
}

func (e Error) FormatError(p xerrors.Printer) error {
	p.Printf("%d %s", e.Status, e.Message)
	e.frame.Format(p)

	return nil
}

func (e Error) Format(f fmt.State, c rune) {
	xerrors.FormatError(e, f, c)
}

func (e Error) Error() string {
	return fmt.Sprint(e)
}

func (dt *DateTime) UnmarshalJSON(src []byte) (err error) {
	str := string(src)
	dt.Time, err = time.Parse(time.RFC3339, str[1:len(str)-1])

	return err
}
