package miro

import (
	"os"
	"path"
	"strconv"
	"strings"

	http "github.com/valyala/fasthttp"
)

// GetPicture returns Picture of requested source type.
func (m *Miro) GetPicture(t, id string, redirect bool, size int) (*Picture, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)
	a.Add("redirect", strconv.FormatBool(redirect))
	a.Add("size", strconv.Itoa(size))

	dst, err := m.get(path.Join(t, id, "picture"), a)
	if err != nil {
		return nil, err
	}

	if len(dst) == 0 {
		return nil, nil
	}

	p := new(Picture)
	if err = m.parser.Unmarshal(dst, p); err != nil {
		return nil, err
	}

	return p, nil
}

// CreateOrUpdatePicture creates or updates exists Picture by input JPEG/PNG file.
func (m *Miro) CreateOrUpdatePicture(image *os.File, t, id string, fields ...string) (*Picture, error) {
	a := http.AcquireArgs()
	defer http.ReleaseArgs(a)

	if len(fields) > 0 {
		a.Add("fields", strings.Join(fields, ","))
	}

	dst, err := m.upload(image, path.Join(t, id, "picture"), a)
	if err != nil {
		return nil, err
	}

	p := new(Picture)
	if err = m.parser.Unmarshal(dst, p); err != nil {
		return nil, err
	}

	return p, nil
}

// DeletePicture remove picture if requested source type.
func (m *Miro) DeletePicture(t, id string) (bool, error) {
	err := m.delete(path.Join(t, id, "picture"))
	return err == nil, err
}
