package miro_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/toby3d/miro"
)

func TestListAllWidgets(t *testing.T) {
	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	list, count, err := client.ListAllWidgets(boardID, miro.WidgetTypeCard)
	assert.NoError(t, err)

	if _, ok = os.LookupEnv("MIRO_WIDGET_ID"); !ok {
		assert.Zero(t, count)
		assert.Empty(t, list)

		return
	}

	assert.NotZero(t, count)
	assert.NotEmpty(t, list)
}

func TestGetWidget(t *testing.T) {
	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	widgetID, ok := os.LookupEnv("MIRO_WIDGET_ID")
	if !ok {
		TestCreateWidget(t)

		widgetID = os.Getenv("MIRO_WIDGET_ID")
	}

	result, err := client.GetWidget(boardID, widgetID)
	assert.NoError(t, err)
	assert.NotEmpty(t, result)
}

func TestCreateWidget(t *testing.T) {
	if _, ok := os.LookupEnv("MIRO_WIDGET_ID"); ok {
		t.SkipNow()
	}

	clientID, ok := os.LookupEnv("MIRO_CLIENT_ID")
	if !ok {
		t.SkipNow()
	}

	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	widget := new(miro.Widget)
	widget.Type = miro.WidgetTypeCard
	widget.Title = "developer card"
	widget.Style = make(map[string]interface{})
	widget.Style["backgroundColor"] = "#ff00ff"
	widget.Capabilities = new(miro.Capabilities)
	widget.Capabilities.Editable = true
	metaData := make(map[string]interface{})
	metaData[clientID] = struct {
		Changed string `json:"changed"`
	}{Changed: "data"}
	widget.Metadata = metaData

	result, err := client.CreateWidget(boardID, *widget)
	if !assert.NoError(t, err) {
		assert.FailNow(t, err.Error())
	}

	if !assert.NotEmpty(t, result) {
		t.FailNow()
	}

	os.Setenv("MIRO_WIDGET_ID", result.ID)
}

func TestUpdateWidget(t *testing.T) {
	widgetID, ok := os.LookupEnv("MIRO_WIDGET_ID")
	if !ok {
		TestCreateWidget(t)

		widgetID = os.Getenv("MIRO_WIDGET_ID")
	}

	clientID, ok := os.LookupEnv("MIRO_CLIENT_ID")
	if !ok {
		t.SkipNow()
	}

	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	widget := new(miro.Widget)
	widget.Title = "new title"
	widget.Assignee = nil
	widget.Style = make(map[string]interface{})
	widget.Style["backgroundColor"] = "#0000ff"
	widget.Capabilities = new(miro.Capabilities)
	widget.Capabilities.Editable = true

	metaData := make(map[string]interface{})
	metaData[clientID] = struct {
		Changed string `json:"changed"`
	}{Changed: "another-data"}

	widget.Metadata = metaData

	result, err := client.UpdateWidget(boardID, widgetID, *widget)
	if !assert.NoError(t, err) {
		assert.FailNow(t, err.Error())
	}

	if !assert.NotEmpty(t, result) {
		t.FailNow()
	}

	os.Setenv("MIRO_WIDGET_ID", result.ID)
}

func TestDeleteWidget(t *testing.T) {
	boardID, ok := os.LookupEnv("MIRO_BOARD_ID")
	if !ok {
		TestCreateBoard(t)

		boardID = os.Getenv("MIRO_BOARD_ID")
	}

	widgetID, ok := os.LookupEnv("MIRO_WIDGET_ID")
	if !ok {
		TestCreateWidget(t)

		widgetID = os.Getenv("MIRO_WIDGET_ID")
	}

	ok, err := client.DeleteWidget(boardID, widgetID)
	assert.NoError(t, err)
	assert.True(t, ok)
}
