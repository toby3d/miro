//nolint: dupl
package miro

// GetTeamUserConnection returns a Team User Connection if the connection exists and is available to the user.
func (m *Miro) GetTeamUserConnection(id string, fields ...string) (*TeamUserConnection, error) {
	dst, err := m.getUserConnection("team", id, fields...)
	if err != nil {
		return nil, err
	}

	result := new(TeamUserConnection)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// UpdateTeamUserConnection updated Team User Connection of selected user.
//
// The role of the last administrator for your team cannot be changed.
func (m *Miro) UpdateTeamUserConnection(id, newRole string, fields ...string) (*TeamUserConnection, error) {
	dst, err := m.updateUserConnection("team", id, newRole, fields...)
	if err != nil {
		return nil, err
	}

	result := new(TeamUserConnection)
	if err = m.parser.Unmarshal(dst, result); err != nil {
		return nil, err
	}

	return result, nil
}

// DeleteTeamUserConnection removes User Connection to the Team.
//
// The last connection cannot be deleted with the ADMIN role in the team.
//
// If the removed user owns any boards or projects, they also will be removed.
// In case you want to save them, you need to reassign ownership first.
func (m *Miro) DeleteTeamUserConnection(id string) (bool, error) {
	return m.deleteUserConnection("team", id)
}
